package handlers

import (
	"net/http"

	"gitlab.com/cloudany-go/gateway/common"

	"github.com/gin-gonic/gin"
)

// HealthResponse ...
type HealthResponse struct {
	Name    string `json:"name"`
	Version string `json:"version"`
	Uptime  string `json:"uptime"`
}

// NewHealthHandler make a gin compatible health check handler
// It take couple parameters (name, version) to provide health checking report
func NewHealthHandler(name, version string) gin.HandlerFunc {
	start := common.NowFunc()
	baseRsp := HealthResponse{
		Name:    name,
		Version: version,
		Uptime:  "",
	}

	return func(c *gin.Context) {
		rsp := baseRsp
		rsp.Uptime = common.NowFunc().Sub(start).String()
		c.JSON(http.StatusOK, rsp)
	}
}
