package handlers

import (
	"gitlab.com/cloudany-go/gateway/common"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

func RequestID() gin.HandlerFunc {
	return func(c *gin.Context) {
		requestID := c.Request.Header.Get(common.XRequestIDKey)
		if requestID == "" {
			uuid4, _ := uuid.NewRandom()
			requestID = strings.Replace(uuid4.String(), "-", "", -1)
		}

		// Expose it for use in the application
		c.Set(common.XRequestIDKey, requestID)

		c.Writer.Header().Set(common.XRequestIDKey, requestID)

		c.Next()
	}
}
