package handlers

import (
	"net/http/httptest"
	"testing"

	"gitlab.com/cloudany-go/gateway/common"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestRequestID(t *testing.T) {
	test := func(clientReqID string) {
		handler := RequestID()
		out := httptest.NewRecorder()
		ctx, _ := gin.CreateTestContext(out)
		ctx.Request = httptest.NewRequest("GET", "/api/status", nil)

		if clientReqID != "" {
			ctx.Request.Header.Set(common.XRequestIDKey, clientReqID)
		}

		handler(ctx)

		assert.NotEmpty(t, out.Header().Get(common.XRequestIDKey))

		if clientReqID != "" {
			assert.Equal(t, clientReqID, out.Header().Get(common.XRequestIDKey))
		}
	}

	test("")
	test("client-set-request-id")
}
