package handlers

import (
	"bytes"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestRequestLog(t *testing.T) {
	buf := new(bytes.Buffer)

	handler := RequestLog("dev", "jx", "rc", buf)
	out := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(out)
	ctx.Request = httptest.NewRequest("GET", "/api/test?query=test", nil)
	handler(ctx)
	<-time.After(time.Millisecond * 100) // due the logger is running in goroutine
	assert.NotEmpty(t, buf.String())
}
