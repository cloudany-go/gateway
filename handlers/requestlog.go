package handlers

import (
	"fmt"
	"io"
	"strconv"
	"strings"
	"sync"
	"time"

	"gitlab.com/cloudany-go/gateway/common"
	"gitlab.com/cloudany-go/gateway/dogstats"

	"github.com/gin-gonic/gin"
)

const space = " "

// RequestLog ...
func RequestLog(env, team, appName string, writer io.Writer) gin.HandlerFunc {
	pool := &sync.Pool{
		New: func() interface{} {
			return new(strings.Builder)
		},
	}

	return func(c *gin.Context) {
		start := time.Now()
		c.Next()

		go func(c *gin.Context) {
			w := pool.Get().(*strings.Builder)
			w.Reset()

			latency := time.Since(start)

			path := c.Request.URL.Path
			if c.Request.URL.RawQuery != "" {
				path = path + "?" + c.Request.URL.RawQuery
			}

			var (
				b          [64]byte
				sliceB     = b[:0]
				statusCode = strconv.Itoa(c.Writer.Status())
				upstream   = c.GetString("upstream")
				userID     = c.GetString(common.XUserIDKey)
				auth       = "false"
			)
			sliceB = time.Now().AppendFormat(sliceB, time.RFC3339)

			w.WriteString(`level=info time="` + string(sliceB) + `" `)
			w.WriteString("id=" + c.GetString(common.XRequestIDKey) + space)
			w.WriteString("method=" + c.Request.Method + space)
			w.WriteString(`path="` + path + `" `)
			w.WriteString(`user-agent="` + c.Request.UserAgent() + `" `)
			if userID != "" {
				w.WriteString("user=" + userID + space)
				auth = "true"
			}
			w.WriteString("latency=" + strconv.FormatInt(latency.Milliseconds(), 10) + space)
			if upstream != "" {
				w.WriteString("upstream=" + upstream + space)
			}
			w.WriteString("code=" + statusCode)
			w.WriteString(` x-forward-for="` + c.Request.Header.Get("x-forward-for") + `" ` + "\n")

			_, _ = fmt.Fprint(writer, w.String())
			pool.Put(w)

			pathTag := c.Request.URL.RequestURI()
			if key := c.GetString("route.key"); key != "" {
				pathTag = key
			}

			// add metric
			tags := []string{
				"env:" + env,
				"path:" + pathTag,
				"upstream:" + upstream,
				"status:" + statusCode,
				"method:" + c.Request.Method,
				"team:" + team,
				"auth:" + auth,
				"app_name:" + appName,
			}

			_ = dogstats.Count("gw.requests.count", 1, tags, 1)
			_ = dogstats.Histogram("gw.requests.upstream_latency", float64(latency.Milliseconds()), tags, 1)
		}(c.Copy())
	}
}
