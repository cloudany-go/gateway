package handlers

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"gitlab.com/cloudany-go/gateway/common"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestHealthHandler(t *testing.T) {
	mockedNow := time.Date(2020, 10, 1, 0, 0, 0, 0, time.UTC)
	common.NowFunc = func() time.Time {
		return mockedNow
	}

	handler := NewHealthHandler("echo", "v1.0.0")
	out := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(out)

	mockedNow = mockedNow.Add(time.Second * 10)
	handler(ctx)

	assert.Equal(t, http.StatusOK, out.Code)

	resp := HealthResponse{}
	err := json.Unmarshal(out.Body.Bytes(), &resp)
	require.NoError(t, err)
	assert.Equal(t, "echo", resp.Name)
	assert.Equal(t, "v1.0.0", resp.Version)
	assert.Equal(t, "10s", resp.Uptime)
}
