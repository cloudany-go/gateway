package handlers

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"gitlab.com/cloudany-go/gateway/common"
)

func LogFromGinCtx(tag string, ctx *gin.Context) *logrus.Entry {
	l := logrus.WithField("tag", tag)
	if reqID := ctx.GetString(common.XRequestIDKey); reqID != "" {
		l = l.WithField(common.XRequestIDKey, reqID)
	}
	return l
}
