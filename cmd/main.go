package main

import (
	"context"
	"flag"
	"fmt"
	"io"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
	"time"

	"gitlab.com/cloudany-go/gateway/dogstats"
	"gitlab.com/cloudany-go/gateway/handlers"
	"gitlab.com/cloudany-go/gateway/registry"

	"github.com/caarlos0/env/v6"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"

	_ "net/http/pprof"
)

type appConfig struct {
	AppEnv          string `env:"APP_ENV" envDefault:"stg"`
	ListenAddress   string `env:"LISTEN_ADDRESS" envDefault:"0.0.0.0:8080"`
	Oauth2VerifyURL string `env:"OAUTH2_VERIFY_URL" envDefault:"http://user/api/oauth2/verify"`
	UpstreamTimeout int    `env:"UPSTREAM_TIMEOUT" envDefault:"5"`
	StatsdAddr      string `env:"STATSD_ADDR"` // address to dogstatsd agent
	StatsdPort      int    `env:"STATSD_PORT" envDefault:"8125"`
	Team            string `env:"STATS_TEAM" envDefault:"jx"`
	AppName         string `env:"STATS_APP_NAME" envDefault:"rc"`
	DebugPort       int    `env:"DEBUG_PORT" envDefault:"6060"`
}

var (
	serviceName    = "gateway"
	serviceVersion = "0.0.0"

	config = new(appConfig)
	router *gin.Engine

	action string // CLI action, could be run or validate
)

type WriteFunc func([]byte) (int, error)

func (fn WriteFunc) Write(data []byte) (int, error) {
	return fn(data)
}
func newGinDebugWriter() io.Writer {
	return WriteFunc(func(data []byte) (int, error) {
		logrus.Debugf("%s", data)
		return len(data), nil
	})
}

func init() {
	if os.Getenv("LOG_FORMAT") == "text" {
		logrus.SetFormatter(&logrus.TextFormatter{TimestampFormat: time.RFC3339, FullTimestamp: true})
	} else {
		logrus.SetFormatter(&logrus.JSONFormatter{TimestampFormat: time.RFC3339})
	}
	logrus.SetOutput(os.Stdout)
	envLevel := os.Getenv("LOG_LEVEL")
	level, err := logrus.ParseLevel(envLevel)
	if err != nil {
		level = logrus.InfoLevel
	}
	logrus.SetLevel(level)

	if err := env.Parse(config); err != nil {
		logrus.WithError(err).Fatal("failed to load configuration")
		os.Exit(1)
	}

	flag.StringVar(&action, "action", "run", "action ('run' to start http server, 'validate' to verify onboard services config")
	flag.Parse()

	gin.DefaultWriter = newGinDebugWriter()
}

func startServer() {
	srv := &http.Server{
		Addr:    config.ListenAddress,
		Handler: router,
	}
	log := logrus.WithField("func", "main.startServer")

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.WithError(err).Error("failed to serve")
		}
	}()
	log.WithField("address", config.ListenAddress).Infof("listening")

	// start debug server
	go func() {
		address := fmt.Sprintf("0.0.0.0:%d", config.DebugPort)
		log.Infof("start debug server at %s", address)
		if err := http.ListenAndServe(address, nil); err != nil {
			log.Errorf("failed to serve (debug server): %s", err.Error())
			return
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Info("exiting ...")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	_ = srv.Shutdown(ctx)
}

func makeCors() gin.HandlerFunc {
	corsConfig := cors.DefaultConfig()
	corsConfig.AllowAllOrigins = true
	corsConfig.AddAllowHeaders("*")
	corsConfig.AddExposeHeaders("x-last-page", "x-next-page", "x-page", "x-per-page", "x-total-items")

	return cors.New(corsConfig)
}

func registerServices() error {

	log := logrus.WithField("func", "main.registerServices")

	// init dogstats agent
	if "" != config.StatsdAddr {
		dogstats.Init(fmt.Sprintf("%s:%d", config.StatsdAddr, config.StatsdPort))
	}

	router = gin.New()

	// register middlewares
	router.Use(
		makeCors(),
		handlers.RequestID(),
		handlers.RequestLog(config.AppEnv, config.Team, config.AppName, os.Stdout),
	)

	// health check endpoint
	healthcheck := handlers.NewHealthHandler(serviceName, serviceVersion)
	router.GET("/", healthcheck)
	router.GET("/api/status", healthcheck)

	authHttpClient := &http.Client{
		Timeout: time.Duration(config.UpstreamTimeout) * time.Second,
	}
	authPlugin := registry.NewAuthPlugin(config.Oauth2VerifyURL, authHttpClient)
	httpClient := &http.Client{
		Timeout: time.Duration(config.UpstreamTimeout) * time.Second,

		// don't redirect upstreams automatically
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},

		Transport: &http.Transport{
			DisableKeepAlives:  true,
			DisableCompression: true,
		},
	}
	servicesRegistry := registry.NewRegistry(router, authPlugin.GetAuthorizer(), httpClient)

	servicesFiles, err := filepath.Glob("./config/services/*.yml")
	if err != nil {
		log.WithError(err).Error("failed to scan services folder")
		return err
	}

	// load services from files
	for _, serviceFile := range servicesFiles {
		slog := log.WithField("file", serviceFile)

		apiService, err := registry.LoadServiceFromFile(serviceFile)
		if err != nil {
			slog.WithError(err).Error("failed to load service")
			continue
		}
		slog.Debug("load service config success")
		err = servicesRegistry.RegisterService(apiService)
		if err != nil {
			slog.WithError(err).Error("failed to register service")
			return err
		}
	}
	return nil
}

func main() {

	if err := registerServices(); err != nil {
		logrus.WithError(err).Error("register services failed")
		os.Exit(1)
	}

	logrus.Debug("[main] validate services successfully")

	if action == "run" {
		startServer()
	}
}
