module gitlab.com/cloudany-go/gateway

go 1.14

require (
	github.com/DataDog/datadog-go v4.0.0+incompatible
	github.com/caarlos0/env/v6 v6.2.2
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/google/uuid v1.1.1
	github.com/opentracing/opentracing-go v1.2.0 // indirect
	github.com/philhofer/fwd v1.1.1 // indirect
	github.com/sirupsen/logrus v1.6.0
	github.com/stretchr/testify v1.5.1
	golang.org/x/time v0.0.0-20201208040808-7e3f01d25324 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	gopkg.in/DataDog/dd-trace-go.v1 v1.28.0
	gopkg.in/yaml.v2 v2.2.8
)
