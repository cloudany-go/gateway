package registry

import (
	"bytes"
	"context"
	"encoding/json"
	"gitlab.com/cloudany-go/gateway/handlers"
	"io"
	"net/http"
	"strings"
	"time"

	"gitlab.com/cloudany-go/gateway/common"

	"github.com/gin-gonic/gin"
)

// VerifyRequest data structure to send to verify
type VerifyRequest struct {
	Token  string `json:"token"`
	Object string `json:"object"`
	Action string `json:"action"`
}

// VerifyResponse verifying response data
type VerifyResponse struct {
	Code       int    `json:"code"`
	UserID     string `json:"user_id"`
	Error      string `json:"error"`
	CustomerID string `json:"customer_id"`
	DeviceID   string `json:"device_id"`
}

// AuthPlugin plugin handles authorization
// It has a method to get authorizer which will contact to authx service
// to authorize the request and set respective headers to context
type AuthPlugin interface {
	GetAuthorizer() gin.HandlerFunc
}

type authPluginImpl struct {
	verifyURL  string
	httpClient HttpClient
}

const (
	authPluginUserAgent = "gateway-auth/1.0"

	authHeaderKey = "authorization"
)

// GetAuthorizer get the middleware
func (a *authPluginImpl) GetAuthorizer() gin.HandlerFunc {
	return a.authorize
}

func (a *authPluginImpl) terminate(c *gin.Context, code int, reason string) {
	c.JSON(code, gin.H{"error": reason})
	c.Abort()
}

func (a *authPluginImpl) authorize(c *gin.Context) {
	log := handlers.LogFromGinCtx("authPlugin", c).WithField("path", c.Request.URL.String())

	// extract token
	token := ""
	authHeader := c.GetHeader(authHeaderKey)
	if authHeader != "" {
		token = strings.Replace(authHeader, "Bearer ", "", 1)
	}

	if token == "" {
		token = c.Query("token")
	}

	if token == "" {
		log.Warn("empty token")
		a.terminate(c, http.StatusUnauthorized, "invalid authorization header")
		return
	}

	reqData := &VerifyRequest{
		Token:  token,
		Object: c.GetString("auth_op.object"),
		Action: c.GetString("auth_op.action"),
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	bPayload, _ := json.Marshal(reqData)
	req, _ := http.NewRequestWithContext(ctx, "POST", a.verifyURL, bytes.NewReader(bPayload))
	req.Header.Set("user-agent", authPluginUserAgent)

	// forward x-request-id
	req.Header.Set(common.XRequestIDKey, c.GetString(common.XRequestIDKey))

	log.WithField("payload", reqData).
		WithField("headers", req.Header).
		Debug("request to authx")

	rsp, err := a.httpClient.Do(req)
	if err != nil {
		log.WithError(err).Error("failed to communicate with #ms-user")
		a.terminate(c, http.StatusInternalServerError, "failed to connect authorizer")
		return
	}

	if rsp.StatusCode != http.StatusOK {
		a.terminate(c, rsp.StatusCode, "error while authorizing, unexpected responded status code")
		return
	}
	rspData := &VerifyResponse{}
	maxSize := 1024 * 4
	rawResponse := make([]byte, maxSize)
	n, err := rsp.Body.Read(rawResponse)
	if err != nil && io.EOF != err {
		log.WithError(err).Error("failed to read authx response")
		a.terminate(c, http.StatusInternalServerError, "failed to connect authorizer")
		return
	}

	// just warning, consider to be error later
	if n >= maxSize {
		log.Warn("oversize responded body")
	}
	rawResponse = rawResponse[:n]
	log.
		WithField("request.object", reqData.Object).
		WithField("request.action", reqData.Action).
		WithField("response", string(rawResponse)).
		Debug("authx request")
	_ = rsp.Body.Close()

	err = json.Unmarshal(rawResponse, rspData)
	if err != nil {
		log.WithError(err).Error("unable to decode ms-user response (invalid json)")
		a.terminate(c, http.StatusInternalServerError, "error while authenticating (invalid json)")
		return
	}

	if !a.isAuthorizedResponse(rspData) {
		a.terminate(c, rspData.Code, rspData.Error)
		return
	}

	// authorized, set context values and go ahead
	// remove authorization header
	c.Request.Header.Del(authHeaderKey)
	c.Set(common.XUserIDKey, rspData.UserID)

	// customized on central project
	if "" != rspData.CustomerID {
		c.Set(common.XCustomerIDKey, rspData.CustomerID)
	}
	if "" != rspData.DeviceID {
		c.Set(common.XDeviceIDKey, rspData.DeviceID)
	}

	c.Next()
}

func (a *authPluginImpl) isAuthorizedResponse(rspData *VerifyResponse) bool {
	return rspData.Code == http.StatusOK
}

// NewAuthPlugin make new plugin instance
func NewAuthPlugin(verifyURL string, client HttpClient) AuthPlugin {
	plug := &authPluginImpl{
		verifyURL:  verifyURL,
		httpClient: client,
	}

	return plug
}
