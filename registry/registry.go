package registry

import (
	"bytes"
	"errors"
	"fmt"
	"gitlab.com/cloudany-go/gateway/handlers"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"gitlab.com/cloudany-go/gateway/common"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

//go:generate mockery -name HttpClient

// Registry provides storage for services and routes
type Registry interface {
	// RegisterService register all service's routes into gin Engine
	RegisterService(s *Service) error
}

// HttpClient ...
type HttpClient interface {
	Do(req *http.Request) (*http.Response, error)
}

type registryImpl struct {
	engine         *gin.Engine
	authMiddleware gin.HandlerFunc
	client         HttpClient
}

func (reg *registryImpl) terminate(c *gin.Context, code int, reason string) {
	c.JSON(code, gin.H{"error": reason})
	c.Abort()
}

// getUpstreamPath builds the path to call upstream
func (reg *registryImpl) getUpstreamPath(c *gin.Context, route *Route) string {
	rPath := c.Request.URL.Path
	if !route.StripSID {
		// remove service ID in rPath
		rPath = strings.Replace(rPath, "/"+route.S.ID, "", 1)
	}
	if "" != c.Request.URL.RawQuery {
		rPath += "?" + c.Request.URL.RawQuery
	}

	return route.S.Host + rPath
}

func (reg *registryImpl) request(c *gin.Context, route *Route) {
	log := handlers.LogFromGinCtx("registryImpl.request", c).WithField("service", route.S.ID)

	url := reg.getUpstreamPath(c, route)

	var body io.Reader = nil
	if c.Request.Body != nil {
		safeReader := io.LimitReader(c.Request.Body, 1024*1024*4) // support maximum 4MB payload
		bytesBody, err := ioutil.ReadAll(safeReader)
		if err != nil {
			log.WithError(err).Error("failed to read request body")
			reg.terminate(c, http.StatusInternalServerError, "read body error")
			return
		}
		body = bytes.NewReader(bytesBody)
		_ = c.Request.Body.Close()
	}

	log.
		WithField("url", url).
		WithField("path", c.Request.URL.String()).
		WithField("method", c.Request.Method).
		Debug("prepare upstream request")

	req, err := http.NewRequestWithContext(c, route.Method, url, body)
	if err != nil {
		reg.terminate(c, http.StatusInternalServerError, "failed to init request: "+err.Error())
		return
	}
	// disable chunked encoding
	req.TransferEncoding = []string{"identity"}

	// forward client header to upstream
	for k, v := range c.Request.Header {
		req.Header.Set(k, strings.Join(v, ";"))
	}

	// forward authorized data
	if c.GetString(common.XUserIDKey) != "" {
		req.Header.Set(common.XUserIDKey, c.GetString(common.XUserIDKey))

		if v := c.GetString(common.XCustomerIDKey); "" != v {
			req.Header.Set(common.XCustomerIDKey, v)
		}
		if v := c.GetString(common.XDeviceIDKey); "" != v {
			req.Header.Set(common.XDeviceIDKey, v)
		}
	}
	req.Header.Add("x-trace", "gateway")
	req.Header.Add("user-agent", "gateway/v0.0.0")
	req.Header.Add(common.XRequestIDKey, c.GetString(common.XRequestIDKey))

	// flag the upstream
	c.Set("upstream", route.S.ID)

	start := time.Now()
	rsp, err := reg.client.Do(req)
	latency := time.Since(start)

	// we only produce 'application/json' now
	c.Header("content-type", "application/json")
	c.Header("x-upstream-latency-in-ms", fmt.Sprintf("%d", latency.Milliseconds()))

	if err != nil {
		log.WithError(err).Errorf("failed to connect upstream")
		reg.terminate(c, http.StatusServiceUnavailable, "failed to request to upstream")
		return
	}

	// response
	c.Status(rsp.StatusCode)
	if rsp.Header.Get("x-trace") == "" {
		c.Header("x-trace", "gateway;"+route.S.ID)
	} else {
		c.Header("x-trace", rsp.Header.Get("x-trace"))
	}

	// copy headers
	for key, valList := range rsp.Header {
		c.Header(key, strings.Join(valList, ";"))
	}

	if rsp.Body != nil {
		n, err := io.Copy(c.Writer, rsp.Body)
		if err != nil {
			log.WithError(err).Error("error while copying upstream response")
		} else {
			log.WithField("copied_bytes", n).Debug("copied upstream response bytes")
		}
		_ = rsp.Body.Close()
	}
}

// RegisterService register all service's routes into gin Engine
func (reg *registryImpl) RegisterService(s *Service) error {
	if s.ID == "" {
		return errors.New("empty service ID")
	}

	for _, route := range s.Routes {
		// store a reference to service
		if route.S == nil {
			route.S = s
		}

		reg.registerRoute(route)
	}

	logrus.WithFields(logrus.Fields{
		"service": s.ID,
		"routes":  len(s.Routes),
		"host":    s.Host,
	}).Debug("registered routes")

	return nil
}

func (reg *registryImpl) registerRoute(route *Route) {
	logrus.
		WithField("route", route.GetFinalPath()).
		WithField("method", route.Method).
		WithField("auth_op", route.AuthOp).
		WithField("service", route.S.ID).
		Debug("register route")
	handlers := gin.HandlersChain{reg.setRouteMetaHandler(route)}
	if route.AuthOp != nil {
		handlers = append(handlers, reg.authMiddleware)
	}
	handlers = append(handlers, reg.makeRouteProxyHandler(route))

	reg.engine.Handle(route.Method, route.GetFinalPath(), handlers...)
}

func (reg *registryImpl) makeRouteProxyHandler(r *Route) gin.HandlerFunc {
	return func(c *gin.Context) {
		reg.request(c, r)

		c.Next()
	}
}

func (reg *registryImpl) setRouteMetaHandler(r *Route) gin.HandlerFunc {
	return func(c *gin.Context) {
		if r.AuthOp != nil {
			c.Set("auth_op.object", r.AuthOp.Object)
			c.Set("auth_op.action", r.AuthOp.Action)
		}
		c.Set("route.key", r.GetFinalPath()) // use this key for request tag
		c.Next()
	}
}

// NewRegistry ...
func NewRegistry(engine *gin.Engine, authMiddleware gin.HandlerFunc, client HttpClient) Registry {
	return &registryImpl{
		engine:         engine,
		authMiddleware: authMiddleware,
		client:         client,
	}
}
