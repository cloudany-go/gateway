package registry

import (
	"path/filepath"
	"runtime"
	"testing"

	"github.com/stretchr/testify/suite"
)

type serviceTestSuite struct {
	suite.Suite
	fixturesPath string
}

func (ts *serviceTestSuite) SetupSuite() {
	_, filename, _, _ := runtime.Caller(0)
	ts.fixturesPath = filepath.Join(filepath.Dir(filename), "fixtures")
}

func (ts *serviceTestSuite) TestLoadEchoSuccess() {
	s, err := LoadServiceFromFile(filepath.Join(ts.fixturesPath, "echo.yml"))
	ts.Assert().NoError(err)
	ts.Assert().NotNil(s)
	ts.Assert().Equal("echo", s.ID)
	ts.Assert().Equal(4, len(s.Routes))
	ts.Assert().Equal("GET:/echo", s.Routes[0].ID)
	ts.Assert().Equal("GET:/echo/status", s.Routes[1].ID)
	ts.Assert().Equal("GET:/echo-v1/public", s.Routes[2].ID)
}

func TestService(t *testing.T) {
	suite.Run(t, new(serviceTestSuite))
}
