package registry

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"strings"

	"gopkg.in/yaml.v2"
)

// AuthOp describes an authorization operation
type AuthOp struct {
	Object string `yaml:"object"`
	Action string `yaml:"action"`
}

// Route ...
type Route struct {
	Path     string   `yaml:"path"`
	Method   string   `yaml:"method"`
	StripSID bool     `yaml:"strip_sid"` // service_id stripped (on gateway side)
	AuthOp   *AuthOp  `yaml:"auth_op"`
	S        *Service `yaml:"-"` // back reference to service
	ID       string   `yaml:"-"` // always be METHOD:SERVICE_ID/PATH & will be indexed in map for fast access
}

// Service describes service
type Service struct {
	ID     string   `yaml:"-"` // service id, should be taken from yaml file name
	Routes []*Route `yaml:"routes"`
	Host   string   `json:"host,omitempty"`
}

var (
	readFile = ioutil.ReadFile
)

// LoadServiceFromFile ...
func LoadServiceFromFile(path string) (*Service, error) {
	// read file
	raw, err := readFile(path)
	if err != nil {
		return nil, err
	}
	s := &Service{}
	if err = yaml.Unmarshal(raw, s); err != nil {
		return nil, err
	}

	s.ID = strings.TrimSuffix(filepath.Base(path), filepath.Ext(path))

	// build route ids
	for _, r := range s.Routes {
		// attach service back to route for reference later
		r.S = s

		r.Method = strings.ToUpper(r.Method)
		r.ID = fmt.Sprintf("%s:%s", r.Method, r.GetFinalPath())
	}

	// strip ending slash on host
	s.Host = strings.TrimRight(s.Host, "/")

	return s, nil
}

// GetFinalPath calculates final path
// with service id prefixed if strip_sid is false
func (r *Route) GetFinalPath() string {
	if r.StripSID {
		return r.Path
	}
	p := fmt.Sprintf("/%s%s", r.S.ID, r.Path)

	// strip last "/"
	p = strings.TrimRight(p, "/")

	return p
}
