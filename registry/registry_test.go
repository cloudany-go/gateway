package registry

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/cloudany-go/gateway/common"
	"gitlab.com/cloudany-go/gateway/registry/mocks"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
)

type registryTestSuite struct {
	suite.Suite

	reg *registryImpl
}

func (ts *registryTestSuite) SetupTest() {
	gin.SetMode(gin.TestMode)

	ts.reg = &registryImpl{
		engine: gin.New(),
		authMiddleware: func(c *gin.Context) {
			c.Set(common.XUserIDKey, "1")
			c.Set(common.XCustomerIDKey, "1001")
			c.Set(common.XDeviceIDKey, "x701")
		},
		client: nil,
	}
}

func TestRegistrySuite(t *testing.T) {
	suite.Run(t, new(registryTestSuite))
}

func (ts *registryTestSuite) onboardEcho() {
	s := &Service{
		ID:   "echo",
		Host: "http://echo.stg.service",
		Routes: []*Route{
			{
				Path:     "/status",
				Method:   http.MethodGet,
				StripSID: false,
			},

			{
				Path:     "/public-echo",
				Method:   http.MethodGet,
				StripSID: true,
			},
			{
				Path:     "/protected",
				Method:   http.MethodPost,
				StripSID: false,
				AuthOp: &AuthOp{
					Object: "echo",
					Action: "read",
				},
			},
		},
	}

	err := ts.reg.RegisterService(s)
	ts.Assert().NoError(err)
}

func (ts *registryTestSuite) TestRegisterService() {
	ts.onboardEcho()

	ts.Assert().Equal("/echo/status", ts.reg.engine.Routes()[0].Path)
	ts.Assert().Equal(http.MethodGet, ts.reg.engine.Routes()[0].Method)

	ts.Assert().Equal("/public-echo", ts.reg.engine.Routes()[1].Path)
}

func (ts *registryTestSuite) TestRequest() {
	ts.onboardEcho()
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/echo/status", nil)

	client := &mocks.HttpClient{}
	rsp := &http.Response{
		StatusCode: 200,
		Body:       ioutil.NopCloser(strings.NewReader("test")),
		Header: map[string][]string{
			"X-Custom": {"val1", "val2"},
		},
	}
	client.On("Do", mock.MatchedBy(func(req *http.Request) bool {
		ts.T().Log(req.URL.String())
		return req.URL.String() == "http://echo.stg.service/status"
	})).Return(rsp, nil)

	ts.reg.client = client

	ts.reg.engine.ServeHTTP(w, req)
	ts.Assert().Equal(200, w.Code)
	ts.Assert().Equal("test", w.Body.String())
	ts.Assert().Equal("val1;val2", w.Header().Get("X-Custom"))
}

func (ts *registryTestSuite) TestRequestProtected() {
	ts.onboardEcho()
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/echo/protected", nil)

	client := &mocks.HttpClient{}
	rsp := &http.Response{
		StatusCode: 200,
		Body:       ioutil.NopCloser(strings.NewReader("test")),
		Header: map[string][]string{
			"X-Custom": {"val1", "val2"},
		},
	}
	client.On("Do", mock.MatchedBy(func(req *http.Request) bool {
		ts.T().Log(req.URL.String())

		// assert forwarded authorized info
		ts.Assert().Equal("1", req.Header.Get(common.XUserIDKey))
		ts.Assert().Equal("1001", req.Header.Get(common.XCustomerIDKey))
		ts.Assert().Equal("x701", req.Header.Get(common.XDeviceIDKey))

		return req.URL.String() == "http://echo.stg.service/protected"
	})).Return(rsp, nil)

	ts.reg.client = client

	ts.reg.engine.ServeHTTP(w, req)
	ts.Assert().Equal(200, w.Code)
	ts.Assert().Equal("test", w.Body.String())
	ts.Assert().Equal("val1;val2", w.Header().Get("X-Custom"))
}
