package registry

import (
	"bytes"
	"errors"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/cloudany-go/gateway/registry/mocks"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func init() {
	gin.SetMode(gin.TestMode)
}

func TestAuthPlugin(t *testing.T) {
	mockClient := new(mocks.HttpClient)
	resp := http.Response{
		Body:       makeFakedResponse([]byte(`{"code": 200, "user_id": "1", "customer_id": "1001", "device_id": "x701"}`)),
		StatusCode: 200,
	}
	mockClient.On("Do", mock.AnythingOfType("*http.Request")).Once().Return(&resp, nil)
	plugin := NewAuthPlugin("http://user/verify", mockClient)

	out := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(out)
	ctx.Request = httptest.NewRequest("POST", "/auth/info", nil)
	ctx.Request.Header.Set("authorization", "bearer faked-token")
	plugin.GetAuthorizer()(ctx)

	assert.Equal(t, 200, out.Code)
}

func TestAuthPlugin_StatusFromAuthx(t *testing.T) {
	mockClient := new(mocks.HttpClient)
	resp := http.Response{
		Body:       makeFakedResponse([]byte(`{"code": 401, "user_id": 1}`)),
		StatusCode: 401,
	}
	mockClient.On("Do", mock.AnythingOfType("*http.Request")).Once().Return(&resp, nil)
	plugin := NewAuthPlugin("http://user/verify", mockClient)

	out := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(out)
	ctx.Request = httptest.NewRequest("POST", "/auth/info", nil)
	ctx.Request.Header.Set("authorization", "bearer faked-token")
	plugin.GetAuthorizer()(ctx)

	assert.Equal(t, 401, out.Code)
}

func TestAuthPlugin401EmptyToken(t *testing.T) {
	plugin := NewAuthPlugin("", nil)
	out := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(out)
	ctx.Request = httptest.NewRequest("POST", "/auth/info", nil)
	// ctx.Request.Header.Set("authorization", "bearer faked-token")
	plugin.GetAuthorizer()(ctx)

	assert.Equal(t, 401, out.Code)
}

func TestAuthPlugin500_AuthxConnectError(t *testing.T) {
	mockClient := new(mocks.HttpClient)
	mockClient.On("Do", mock.AnythingOfType("*http.Request")).Once().Return(nil, errors.New("disconnected #ms-user"))
	plugin := NewAuthPlugin("http://user/verify", mockClient)

	out := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(out)
	ctx.Request = httptest.NewRequest("POST", "/auth/info", nil)
	ctx.Request.Header.Set("authorization", "bearer faked-token")
	plugin.GetAuthorizer()(ctx)

	assert.Equal(t, 500, out.Code)
}

func TestAuthPlugin500_AuthxReturnInvalidJSON(t *testing.T) {
	mockClient := new(mocks.HttpClient)
	resp := http.Response{
		Body:       makeFakedResponse([]byte(`{"code": 200, invalid json`)),
		StatusCode: 200,
	}
	mockClient.On("Do", mock.AnythingOfType("*http.Request")).Once().Return(&resp, nil)
	plugin := NewAuthPlugin("http://user/verify", mockClient)

	out := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(out)
	ctx.Request = httptest.NewRequest("POST", "/auth/info", nil)
	ctx.Request.Header.Set("authorization", "bearer faked-token")
	plugin.GetAuthorizer()(ctx)

	assert.Equal(t, 500, out.Code)
}

func makeFakedResponse(p []byte) io.ReadCloser {
	return ioutil.NopCloser(bytes.NewReader(p))
}
