package dogstats

import (
	"errors"

	"github.com/DataDog/datadog-go/statsd"
)

var (
	agent *statsd.Client

	ErrUninitializedAgent = errors.New("uninitialized agent")
)

// Init ...
func Init(addr string) {
	agent, _ = statsd.New(addr)
}

// Count ...
func Count(name string, value int64, tags []string, rate float64) error {
	if agent == nil {
		return ErrUninitializedAgent

	}

	return agent.Count(name, value, tags, rate)
}

func Histogram(name string, value float64, tags []string, rate float64) error {
	if agent == nil {
		return ErrUninitializedAgent
	}

	return agent.Histogram(name, value, tags, rate)
}
