package dogstats

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDogstatsCount(t *testing.T) {
	err := Count("test", 1, nil, 1.0)
	assert.Error(t, err)

	Init("127.0.0.1:8125")
	err = Count("test", 1, nil, 1.0)
	if err != nil {
		assert.NotContains(t, err.Error(), "uninitialized")
	}
}
