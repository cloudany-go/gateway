# Gateway

[![pipeline status](https://gitlab.com/jx-adt-www/central/ms-gateway/badges/master/pipeline.svg)](https://gitlab.com/jx-adt-www/central/ms-gateway/-/commits/master)
[![coverage report](https://gitlab.com/jx-adt-www/central/ms-gateway/badges/master/coverage.svg)](https://gitlab.com/jx-adt-www/central/ms-gateway/-/commits/master)


This implements API Gateway pattern which actions as a front controller to handle http/websocket request,
do authorization and forward to respective services.

## Onboard New Service

To onboard a new service, make sure you provide (assuming `SVC_NAME` is your service name):

- A new yaml configuration file in `/config/services/SVC_NAME.yml`

Each service configuration has the format:

```yaml
# echo.yml
host: http://127.0.0.1:8080
routes:
  - path: /hello
    method: POST
    strip_sid: false
```

with each route:

- `path`: gateway will use this path to request to target service
- `strip_sid`: indicates that public url of this contains service_id or not.
With above example the public path will be `/echo/hello`, and request to `echo` service will be `/hello`
