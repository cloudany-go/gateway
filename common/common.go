package common

import (
	"time"
)

const (
	// XUserIDKey ...
	XUserIDKey = "x-user-id"

	// XRequestIDKey ...
	XRequestIDKey = "x-request-id"

	XCustomerIDKey = "x-customer-id"
	XDeviceIDKey   = "x-device-id"
)

var (
	NowFunc = time.Now
)

func MakeTrue() bool {
	return true
}
