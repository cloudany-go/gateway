FROM golang:1.14

ARG VERSION=0.0.0

ENV CGO_ENABLED=0 GO111MODULE=on

WORKDIR /src
ADD go.mod /src
ADD go.sum /src

# cache go modules first
RUN go mod download

ADD . /src

RUN go build -ldflags="-s -w -X main.serviceVersion=${VERSION}" -o /app ./cmd/main.go

FROM alpine:3.11
COPY --from=0 /app /usr/bin/app
ADD ./config /app/config
WORKDIR /app

CMD ["/usr/bin/app"]
